package ieeg.portal.ltsd.client;

import ieeg.portal.ltsd.server.LTSDPortalBackendImpl;
import ieeg.portal.ltsd.shared.FieldVerifier;
import ieeg.portal.ltsd.shared.LTSDDataset;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Ltsdgui implements EntryPoint {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);

	private final LTSDPortalBackendAsync ltsdPortalBackend = GWT
			.create(LTSDPortalBackend.class);
	
	private Panel getLTSDTopPanel() {
		HorizontalPanel topPanel = new HorizontalPanel();
		topPanel.setSpacing(20);
		topPanel.add(getLTSDLoginPanel());
		topPanel.add(getLTSDAnalyzePanel());
		return topPanel;
	}
	
	private Panel getLTSDLoginPanel(){
		final VerticalPanel loginPanel = new VerticalPanel();
		
		final Label usernameLabel = new Label("Username:");
		final TextBox usernameField = new TextBox();
		final Label passwordLabel = new Label("Password:");
		final TextBox passwordField = new TextBox();
		final Label domainLabel = new Label("Domain URL:");
		final TextBox domainField = new TextBox();
		final Label studynameLabel = new Label("Study Name:");
		final TextBox studynameField = new TextBox();
		
		final Button loginButton = new Button("LOGIN");
		
	
		Widget[] widgets = {
			usernameLabel,
			usernameField,
			passwordLabel,
			passwordField,
			domainLabel,
			domainField,
			studynameLabel,
			studynameField,
			loginButton
		};
		
		for (int i = 0; i < widgets.length; i++)
			loginPanel.add(widgets[i]);
		
		
		class LoginHandler implements ClickHandler, KeyUpHandler {
			/**
			 * Fired when the user clicks on the sendButton.
			 */
			public void onClick(ClickEvent event) {
				sendLoginDetailsToServer();
			}

			/**
			 * Fired when the user types in the nameField.
			 */
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					sendLoginDetailsToServer();
				}
			}	
			

			/**
			 * Send the name from the nameField to the server and wait for a response.
			 * 
			 * Does this by firing an event on the event bus, and passing along a
			 * handler for the response.
			 */
			private void sendLoginDetailsToServer() {
				
				// First, we validate the input.
				//TODO display lack of error on the bottom
				//errorLabel.setText("");
				final String userName = usernameField.getText();
				final String password = passwordField.getText();
				final String domain = domainField.getText();
				final String studyName = studynameField.getText();

				// Then, we send the input to the server.
				loginButton.setEnabled(false);

				ltsdPortalBackend.getStudyFromLogin(userName, password, domain, studyName, new AsyncCallback<String>() {
						@Override
						public void onFailure(Throwable caught){
							System.out.println("Failure");
							
						}
						@Override
						public void onSuccess(String result){
							System.out.println("Success");
							System.out.println(userName);
						}

				});
			}
		}
		
		loginButton.addClickHandler(new LoginHandler());
		return loginPanel;
	}
	
	private Panel getLTSDAnalyzePanel() {
		HorizontalPanel dividerPanel = new HorizontalPanel();
		dividerPanel.setSpacing(30);
		dividerPanel.add(getLTSDInfoPanel());
		dividerPanel.add(getLTSDSelectionPanel());
		return dividerPanel;
	}
	
	private Panel getLTSDInfoPanel() {
		VerticalPanel infoPanel = new VerticalPanel();
		
		Label experimentLabel = new Label("Experiment:");
		Label experimentValue = new Label("exp_012_9.28.2013");
		Label patientLabel = new Label("Patient:");
		Label patientValue = new Label("Subject #312");
		Label dateLabel = new Label("Date:");
		Label dateValue = new Label("9/28/2013");
		Label lengthLabel = new Label("Data length (hours):");
		Label lengthValue = new Label("12.34");
		
		Widget[] widgets = {
			experimentLabel,
			experimentValue,
			patientLabel,
			patientValue,
			dateLabel,
			dateValue,
			lengthLabel,
			lengthValue
		};
		
		for (int i = 0; i < widgets.length; i++)
			infoPanel.add(widgets[i]);
		
		return infoPanel;
	}
	
	private Panel getLTSDSelectionPanel() {
		VerticalPanel selectionPanel = new VerticalPanel();
		
		Label annotationsLabel = new Label("Number of annotations:");
		Label annotationsValue = new Label("305");
		Label seizuresLabel = new Label("Number of seizure annotations:");
		Label seizuresValue = new Label("267");
		
		FlowPanel selectionWrapper = new FlowPanel();
		selectionWrapper.add(new RadioButton("selection", "Time"));
		selectionWrapper.add(new RadioButton("selection", "Index"));
		
		TextBox fromBox = new TextBox();
		TextBox toBox = new TextBox();
		
		fromBox.setWidth("15px");
		toBox.setWidth("15px");
		
		selectionWrapper.add(fromBox);
		selectionWrapper.add(new Label(" to "));
		selectionWrapper.add(toBox);
		
		Button analyzeButton = new Button("ANALYZE");
		
		Widget[] widgets = {
			annotationsLabel,
			annotationsValue,
			seizuresLabel,
			seizuresValue,
			selectionWrapper,
			analyzeButton
		};
		
		for (int i = 0; i < widgets.length; i++)
			selectionPanel.add(widgets[i]);
		
		return selectionPanel;
	}
	
	private Panel getLTSDMatrixPanel() {
		SimplePanel matrixPanel = new SimplePanel();
		
		double[][] simMatrix = getSimilarityMatrix();
		StringBuffer htmlTable = new StringBuffer();
		htmlTable.append("<table align=\"center\">");
		for (int r = 0; r < simMatrix.length; r++) {
			htmlTable.append("<tr>");
			for (int c = 0; c < simMatrix.length; c++) {
				htmlTable.append("<td>" + simMatrix[r][c] + "</td>");
			}
			htmlTable.append("</tr>");
		}
		htmlTable.append("</table>");
		matrixPanel.add(new HTML(htmlTable.toString()));
		
		return matrixPanel;
	}
	
	private double[][] getSimilarityMatrix() {
		// Should make a request to the server back-end for matrix data later
		
		final int numElements = 15; 
		double[][] simMatrix = new double[numElements][numElements];
		for (int r = 0; r < numElements; r++)
			for (int c = 0; c < numElements; c++) {
				if (r == c)
					simMatrix[r][c] = 1.0;
				else
					simMatrix[r][c] = 0.455;
			}
		return simMatrix;
	}
	
	private Panel getLTSDDendroPanel() {
		SimplePanel dendroPanel = new SimplePanel();
		String url = "http://www.cs.nyu.edu/courses/summer08/"
				+ "G22.3033-002/fig_dendrogram.jpg";
		dendroPanel.add(new HTML("<img src=\"" + url + "\" width=\"400\""
				+ "height=\"400\"/>"));
		return dendroPanel;
	}
	
	private Panel getLTSDStatusPanel() {
		SimplePanel statusPanel = new SimplePanel(); 
		statusPanel.add(new Label("Status: example status"));
		return statusPanel;
	}

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		final DockPanel dockPanel = new DockPanel();
		dockPanel.add(getLTSDTopPanel(), DockPanel.NORTH);
		dockPanel.add(getLTSDStatusPanel(), DockPanel.SOUTH);
		dockPanel.add(getLTSDMatrixPanel(), DockPanel.WEST);
		dockPanel.add(getLTSDDendroPanel(), DockPanel.EAST);

		final Button sendButton = new Button("Send");
		final TextBox nameField = new TextBox();
		nameField.setText("GWT User");
		final Label errorLabel = new Label();

		// We can add style names to widgets
		sendButton.addStyleName("sendButton");

		// Add the nameField and sendButton to the RootPanel
		// Use RootPanel.get() to get the entire body element
		//RootPanel.get("nameFieldContainer").add(nameField);
		//RootPanel.get("sendButtonContainer").add(sendButton);
		//RootPanel.get("errorLabelContainer").add(errorLabel);
		RootPanel.get("mainWindowContainer").add(dockPanel);

		// Focus the cursor on the name field when the app loads
		nameField.setFocus(true);
		nameField.selectAll();

		// Create the popup dialog box
		final DialogBox dialogBox = new DialogBox();
		dialogBox.setText("Remote Procedure Call");
		dialogBox.setAnimationEnabled(true);
		final Button closeButton = new Button("Close");
		// We can set the id of a widget by accessing its Element
		closeButton.getElement().setId("closeButton");
		final Label textToServerLabel = new Label();
		final HTML serverResponseLabel = new HTML();
		VerticalPanel dialogVPanel = new VerticalPanel();
		dialogVPanel.addStyleName("dialogVPanel");
		dialogVPanel.add(new HTML("<b>Sending name to the server:</b>"));
		dialogVPanel.add(textToServerLabel);
		dialogVPanel.add(new HTML("<br><b>Server replies:</b>"));
		dialogVPanel.add(serverResponseLabel);
		dialogVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
		dialogVPanel.add(closeButton);
		dialogBox.setWidget(dialogVPanel);

		// Add a handler to close the DialogBox
		closeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				dialogBox.hide();
				sendButton.setEnabled(true);
				sendButton.setFocus(true);
			}
		});

		// Create a handler for the sendButton and nameField
		class MyHandler implements ClickHandler, KeyUpHandler {
			/**
			 * Fired when the user clicks on the sendButton.
			 */
			public void onClick(ClickEvent event) {
				sendNameToServer();
			}

			/**
			 * Fired when the user types in the nameField.
			 */
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					sendNameToServer();
				}
			}

			/**
			 * Send the name from the nameField to the server and wait for a response.
			 */
			private void sendNameToServer() {
				// First, we validate the input.
				errorLabel.setText("");
				String textToServer = nameField.getText();
				if (!FieldVerifier.isValidName(textToServer)) {
					errorLabel.setText("Please enter at least four characters");
					return;
				}

				// Then, we send the input to the server.
				sendButton.setEnabled(false);
				textToServerLabel.setText(textToServer);
				serverResponseLabel.setText("");
				greetingService.greetServer(textToServer,
						new AsyncCallback<String>() {
							public void onFailure(Throwable caught) {
								// Show the RPC error message to the user
								dialogBox
										.setText("Remote Procedure Call - Failure");
								serverResponseLabel
										.addStyleName("serverResponseLabelError");
								serverResponseLabel.setHTML(SERVER_ERROR);
								dialogBox.center();
								closeButton.setFocus(true);
							}

							public void onSuccess(String result) {
								dialogBox.setText("Remote Procedure Call");
								serverResponseLabel
										.removeStyleName("serverResponseLabelError");
								serverResponseLabel.setHTML(result);
								dialogBox.center();
								closeButton.setFocus(true);
							}
						});
			}
		}

		// Add a handler to send the name to the server
		MyHandler handler = new MyHandler();
		sendButton.addClickHandler(handler);
		nameField.addKeyUpHandler(handler);
	}
}
